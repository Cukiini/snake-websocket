# docker build -t test1 .
# docker run --rm --name test1 -p 80:80 test1
FROM node:12-alpine as builder

WORKDIR /app
COPY package.json .
COPY package-lock.json .
RUN npm ci
COPY . .
RUN npm run build
RUN npm ci --production

FROM node:12-alpine

WORKDIR /app
COPY --from=builder /app /app

ENV NODE_ENV=production
ENV PORT=80

CMD [ "node", "dist/server/main.js" ]
