/* eslint-disable @typescript-eslint/interface-name-prefix */
export interface ICoordinate {
  x: number;
  y: number;
}

export type ISnakeDirection = 'down' | 'up' | 'left' | 'right';

export interface ISnake {
  color: string;
  playerId: string;
  direction?: ISnakeDirection;
  coordinates: ICoordinate[];
}

export interface ITarget {
  id: string;
  coordinate: ICoordinate;
  expires: number;
}
